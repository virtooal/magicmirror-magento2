<?php
/**
 * Virtooal TryOn Mirror Integration
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@sostanza.it so we can send you a copy immediately.
 *
 * @category  Virtooal
 * @package   Virtooal_TryOnMirror
 * @copyright Copyright 2018 Eureco s.r.o. (http://www.virtooal.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Virtooal\TryOnMirror\Observer;

class LayoutLoadBefore implements \Magento\Framework\Event\ObserverInterface
{
    const POSITION_UNDERIMAGE = 'product_view_media';
    const POSITION_UNDERCART = 'product_view-product.info.addtocart';
    const POSITION_UNDERADDTO = 'product_view-product.info.addto';
    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;

    /**
     * @var \Virtooal\TryOnMirror\Helper\Data
     */
    private $helperData;

    public function __construct(
        \Magento\Framework\Registry $registry,
        \Virtooal\TryOnMirror\Helper\Data $helperData
    ) {
        $this->x = $registry;
        $this->helperData = $helperData;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $action = $observer->getData('full_action_name');
        if ($action != 'catalog_product_view') {
            return $this;
        }

        $position = $this->helperData->getConfig('tryonmirror/general/position');

        if (!$position) {
            return $this;
        }
        $layout = $observer->getLayout();
        $layout->getUpdate()->addHandle('catalog_product_view_'.$position);

        return $this;
    }
}
