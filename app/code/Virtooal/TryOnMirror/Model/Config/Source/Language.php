<?php
/**
 * Virtooal Try On Mirror Integration
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@sostanza.it so we can send you a copy immediately.
 *
 * @category  Virtooal
 * @package   Virtooal_TryOnMirror
 * @copyright Copyright 2018 Eureco s.r.o. (http://www.virtooal.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Virtooal\TryOnMirror\Model\Config\Source;

class Language implements \Magento\Framework\Option\ArrayInterface
{
 
    /**
     * Return array of options as value-label pairs, eg. value => label
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            'en' => __('English'),
            'de' => __('Deutch'),
            'fr' => __('French'),
            'pl' => __('Polish'),
            'hu' => __('Hungarian'),
            'cz' => __('Czech'),
            'sk' => __('Slovak'),
        ];
    }
}
