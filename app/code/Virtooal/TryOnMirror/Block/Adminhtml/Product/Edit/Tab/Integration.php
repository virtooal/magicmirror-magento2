<?php
/**
 * Virtooal TryOn Mirror Integration
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@sostanza.it so we can send you a copy immediately.
 *
 * @category  Virtooal
 * @package   Virtooal_TryOnMirror
 * @copyright Copyright 2018 Eureco s.r.o. (http://www.virtooal.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


namespace Virtooal\TryOnMirror\Block\Adminhtml\Product\Edit\Tab;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManagerInterface;

class Integration extends \Magento\Framework\View\Element\Template
{
    private $coreRegistry = null;
    private $storeManager;
    private $helperData;
    private $url;

    public function __construct(
        Context $context,
        Registry $registry,
        StoreManagerInterface $storeManager,
        \Virtooal\TryOnMirror\Helper\Data $helperData,
        \Magento\Framework\Url $url,
        array $data = []
    ) {
        $this->storeManager = $storeManager;
        $this->coreRegistry = $registry;
        $this->helperData = $helperData;
        $this->url = $url;
        
        parent::__construct($context, $data);
    }

    public function getProduct()
    {
        return $this->coreRegistry->registry('current_product');
    }

    public function getProductUrl()
    {
        $productId = $this->getProduct()->getId();
        $storeCode = $this->storeManager->getStore()->getCode();
        
        return $this->url->getUrl('catalog/product/view', ['id' => $productId, '_nosid' => true]);
    }

    public function getImageUrl()
    {
        $product = $this->getProduct();
        return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA)
        . 'catalog/product' . $product->getImage();
    }

    public function getApiKey()
    {
        return $this->helperData->getConfig('tryonmirror/general/apikey');
    }

    public function getLanguage()
    {
        return $this->helperData->getConfig('tryonmirror/general/language')?:'en';
    }
}
