<?php
/**
 * Virtooal Try On Mirror Integration
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@sostanza.it so we can send you a copy immediately.
 *
 * @category  Virtooal
 * @package   Virtooal_TryOnMirror
 * @copyright Copyright 2018 Eureco s.r.o. (http://www.virtooal.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Virtooal\TryOnMirror\Block;

use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use Virtooal\TryOnMirror\Helper\Data;

class TryOnMirror extends Template
{
    private $helperData;
    private $storeManager;
    private $coreRegistry = null;
    private $store;

    public function __construct(
        StoreManagerInterface $storeManager,
        Template\Context $context,
        Data $helperData,
        Registry $registry,
        StoreInterface $store,
        array $data = []
    ) {
        $this->storeManager = $storeManager;
        $this->helperData = $helperData;
        $this->coreRegistry = $registry;
        $this->store = $store;
        
        parent::__construct($context, $data);
    }

    public function getIframeUrl()
    {
        $baseUrl = $this->_storeManager->getStore()->getBaseUrl();
        $language = $this->helperData->getConfig('tryonmirror/general/language')?:'en';
        
        return 'https://setup.virtooal.com/'.$language.'/auth/index?url='.rawurlencode($baseUrl).'&platform=magento';
    }

    public function getProductId()
    {
        $product = $this->coreRegistry->registry('current_product');
        return $product->getId();
    }

    public function getUsername()
    {
        return $this->helperData->getConfig('tryonmirror/general/username');
    }

    public function getApiKey()
    {
        return $this->helperData->getConfig('tryonmirror/general/apikey');
    }

    public function getLangCode()
    {
        return substr($this->store->getLocaleCode(), 0, 2);
    }
    
    public function isEnabled()
    {
        return filter_var($this->helperData->getConfig('tryonmirror/general/enabled'), FILTER_VALIDATE_BOOLEAN);
    }
}
